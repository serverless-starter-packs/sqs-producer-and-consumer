AWSTemplateFormatVersion: "2010-09-09"

Description: |
  Serverless Starter Pack: SQS Producer and Consumer Example
  Creates 2 Lambda functions & IAM Roles
    - Producer (Sends messages to SQS)
    - Consumer (Reads messages from SQS)
  SQS Queue & Queue Policy

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: "Resource Naming Configuration"
        Parameters:
          - pCustomQueueName
          - pCustomConsumerLambdaName
          - pCustomProducerLambdaName
      - Label:
          default: "Lambda Trigger Configuration"
        Parameters:
          - pLambdaEventSourceBatchSize
    ParameterLabels:
      pCustomQueueName:
        default: "Would you want a custom name for the SQS Queue?"
      pCustomProducerLambdaName:
        default: "Would you want a custom name for the Producer Lambda?"
      pCustomConsumerLambdaName:
        default: "Would you want a custom name for the Consumer Lambda?"
      pLambdaEventSourceBatchSize:
        default: "What is the Batch size for the Lambda trigger?"

Parameters:

  pCustomQueueName:
    Description: "Create the queue with a custom name, if left empty AWS will automatically create <Stack Name>-<Resource Name>-<Random>."
    Type: String
    Default: ""

  pCustomProducerLambdaName:
    Description: "Create the producer Lambda with a custom name, if left empty AWS will automatically create <Stack Name>-<Resource Name>-<Random>."
    Type: String
    Default: ""

  pCustomConsumerLambdaName:
    Description: "Create the consumer Lambda with a custom name, if left empty AWS will automatically create <Stack Name>-<Resource Name>-<Random>."
    Type: String
    Default: ""

  pLambdaEventSourceBatchSize:
    Description: "Set the Lambda event source batch size. Value needs to be between 1 - 10 (For the purpose of this starter pack, AWS max value for SQS queues is 10.000)."
    Type: Number
    Default: 1
    ConstraintDescription: "Value needs to be between 1 - 10."
    MinValue: 1
    MaxValue: 10

Conditions:

  cOverwriteQueueName: !Not [ !Equals [ !Ref pCustomQueueName, "" ] ]

  cOverwriteProducerLambdaName: !Not [ !Equals [ !Ref pCustomProducerLambdaName, "" ] ]

  cOverwriteConsumerLambdaName: !Not [ !Equals [ !Ref pCustomConsumerLambdaName, "" ] ]

Resources:

  # First we create the SQS Queue that will bridge the messages between applications.
  # To create an standard SQS Queue there are no required parameters, we only provide a Custom Queue name if parameter has been set, an SQS queue by default is a Standard Queue.
  # for a full list see: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-sqs-queues.html#aws-sqs-queue-name
  rProducerAndConsumerSQSQueue:
    Type: AWS::SQS::Queue
    Properties:
      QueueName: !If
        - cOverwriteQueueName
        - !Ref pCustomQueueName
        - !Ref AWS::NoValue

  # Second we create a queue policy and assign it to the SQS Queue, the policy specifies who is allowed to perform which actions.
  # Note that both the SQS Queue and the function role are referenced for security purposes.
  # !Ref & !GetAtt are CloudFormation built-in functions.
  # The SQS Queue resource returns the full Queue Url upon referenced with ref, for the other resources we specify a specific attribute we want to assign.
  rProducerAndConsumerSQSQueuePolicy:
    Type: AWS::SQS::QueuePolicy
    Properties:
      PolicyDocument:
        Statement:
          - Action:
              - SQS:SendMessage
            Effect: Allow
            Resource: !GetAtt rProducerAndConsumerSQSQueue.Arn
            Principal:
              AWS: !GetAtt rProducerLambdaFunctionRole.Arn
          - Action:
              - SQS:ReceiveMessage
              - SQS:ChangeMessageVisibility
              - SQS:DeleteMessage
              - SQS:GetQueueAttributes
            Effect: Allow
            Resource: !GetAtt rProducerAndConsumerSQSQueue.Arn
            Principal:
              AWS: !GetAtt rConsumerLambdaFunctionRole.Arn
      Queues:
        - !Ref rProducerAndConsumerSQSQueue

  # We create our first Lambda which will be our producer Lambda to send messages to the SQS queue.
  # In order for the Lambda to perform actions we assign it an IAM Role.
  # The code is provided inline, and by default the file name within the Lambda with the code is named "index".
  # Once initiated the Lambda will read the "Message" from the test event and pass that in as the SQS message.
  # We pass the Queue Url to the Lambda function in it's environment variables to be used during execution.
  rProducerLambdaFunction:
    Type: AWS::Lambda::Function
    Properties:
      FunctionName: !If
        - cOverwriteProducerLambdaName
        - !Ref pCustomProducerLambdaName
        - !Ref AWS::NoValue
      Role: !GetAtt rProducerLambdaFunctionRole.Arn
      Handler: index.produce_message
      Runtime: python3.8
      Environment:
        Variables:
          QueueUrl: !Ref rProducerAndConsumerSQSQueue
      Code:
        ZipFile: |
          import boto3
          import os
          import json

          # Get environment variable
          QUEUE_URL = os.environ['QueueUrl']
          SQS_CLIENT = boto3.client('sqs')
          
          def produce_message(event, context):
            message = event["Message"]
            try:
              response = SQS_CLIENT.send_message(
                QueueUrl = QUEUE_URL,
                MessageBody = message
              )

              print("SQS RESPONSE: " + str(response))

              return {
                'statusCode': 200,
                'body': json.dumps(response)
              }

            except Exception as e:
              print("EXCEPTION OCCURED: " + str(e))

  # Next we create an IAM role for the Producer Lambda, with permissions for the function to assume this role, as well as an AWS Managed policy to access CloudWatch.
  rProducerLambdaFunctionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service: lambda.amazonaws.com
            Action:
              - sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

  # We create our second Lambda which will be our consumer lambda to read messages from the SQS queue.
  # In order for the Lambda to perform actions we assign it an Lambda Role.
  # The code is provided inline, and by default the file name within the Lambda with the code is named "index".
  # The Lambda will print out the full message body in CloudWatch Logs.
  rConsumerLambdaFunction:
    Type: AWS::Lambda::Function
    Properties:
      FunctionName: !If
        - cOverwriteConsumerLambdaName
        - !Ref pCustomConsumerLambdaName
        - !Ref AWS::NoValue
      Role: !GetAtt rConsumerLambdaFunctionRole.Arn
      Handler: index.consume_message
      Runtime: python3.8
      Code:
        ZipFile: |
          def consume_message(event, context):
            for record in event['Records']:
              print("SQS Message: " + str(record["body"]))

  # Same as for the Producer Lambda, we create an IAM role for the consumer Lambda, with permissions for the function to assume this role, as well as an AWS Managed policy to access CloudWatch.
  rConsumerLambdaFunctionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service: lambda.amazonaws.com
            Action:
              - sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

  # In order for SQS to trigger the Consumer Lambda when messages arrive we need to create an EventSourceMapping between the SQS Queue and Consumer Lambda.
  rConsumerLambdaFunctionTriggerEvent:
    DependsOn: rProducerAndConsumerSQSQueuePolicy
    Type: AWS::Lambda::EventSourceMapping
    Properties:
      BatchSize: !Ref pLambdaEventSourceBatchSize
      Enabled: true
      EventSourceArn: !GetAtt rProducerAndConsumerSQSQueue.Arn
      FunctionName: !GetAtt rConsumerLambdaFunction.Arn

Outputs:

  ProducerLambdaFunctionArn:
    Description: "Producer Lambda Function Arn"
    Value: !GetAtt rProducerLambdaFunction.Arn

  ProducerLambdaFunctionRoleArn:
    Description: "Producer Lambda Function IAM Role Arn"
    Value: !GetAtt rProducerLambdaFunctionRole.Arn

  ConsumerLambdaFunctionArn:
    Description: "Consumer Lambda Function Arn"
    Value: !GetAtt rConsumerLambdaFunction.Arn

  ConsumerLambdaFunctionRoleArn:
    Description: "Consumer Lambda Function IAM Role Arn"
    Value: !GetAtt rConsumerLambdaFunctionRole.Arn

  ProducerAndConsumerSQSQueueUrl:
    Description: "SQS Queue Url"
    Value: !Ref rProducerAndConsumerSQSQueue

  ProducerAndConsumerSQSQueueArn:
    Description: "SQS Queue Arn"
    Value: !GetAtt rProducerAndConsumerSQSQueue.Arn
