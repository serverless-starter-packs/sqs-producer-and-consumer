# SQS Lambda Producer and Consumer Example

This repository contains CloudFormation code for an SQS Queue and 2 Lambda Functions. The first Lambda Function writes messages to the Queue and the second reads messages from it.

An event source mapping resource manages the invocation of the consumer lambda function.

The python code to write to the queue is included inline in the producer lambda function resource and will read the message from the test event.
e.g. given the following test event in the console, the producer will parse the input and send the value of "Message" to the SQS Queue:
```json
{
  "Message": "Message to send to the SQS Queue."
}
```
The python code included inline in the consumer lambda function resource will print the message contents to stdout & CloudWatch.

IAM permissions to allow writing to and reading from the queue are defined in an SQS Policy resource.

This repository is intended to be an example of how this architecture can be achieved and is not a fully-featured template that can be reused as is.

![image](SQS.png)
